import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import java.io.File;

public class DriverManager {

    public static WebDriver getDriver(String browser){
        switch (browser){
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver").getFile()).getPath());
                return new FirefoxDriver();
            case "opera":
                System.setProperty(
                        "webdriver.opera.driver",
                        new File(DriverManager.class.getResource("/operadriver").getFile()).getPath());
                return new OperaDriver();
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                return new ChromeDriver();
        }
    }
}
                    