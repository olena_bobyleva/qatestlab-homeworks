import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class Homework2 {

    public static final String URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    public static final String LOGIN = "webinar.test@gmail.com";
    public static final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";
    public static WebDriver driver = DriverManager.getDriver("chrome");
    public static WebDriverWait wait = new WebDriverWait(driver,10);

    public static void main(String[] args) {
        driver.manage().window().maximize();
        driver.get(URL);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);

//        ** Script A **

        System.out.println("The 'A' script is executed");

        try {
            logIn();
            logOut();
            System.out.println("The 'A' script was executed without errors");
        } catch (Exception e) {
            System.out.println("The exception " + e + " is occurred during the execution of the 'A' script.");
        }
        System.out.println();
        System.out.println();

//        ** Script B **

        System.out.println("The 'B' script is executed");
        System.out.println();

        try {
            logIn();
            mainMenu();
            logOut();
            System.out.println("The 'B' script was executed without errors");
        } catch (Exception e) {
            System.out.println("The exception " + e + " is occurred during the execution of the 'B' script.");
        }
        driver.quit();
    }



    public static void logIn() {
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys(LOGIN);
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys(PASSWORD);
        WebElement submitButton = driver.findElement(By.name("submitLogin"));
        submitButton.click();

    }

    public static void logOut() {
        waitingById("header_employee_box");
        driver.findElement(By.id("header_employee_box")).click();
        driver.findElement(By.id("header_logout")).click();
    }

    public static void mainMenu() {
        waitingByClassName("maintab");
        List<WebElement> itemsMainMenu = driver.findElements(By.className("maintab"));

//       Selecting items from the main menu

        for (int i = 0; i < itemsMainMenu.size(); i++) {
            String textLink = itemsMainMenu.get(i).getText();
            System.out.println("The selecting of the '" + textLink + "' item of the main menu");
            itemsMainMenu.get(i).click();
            waitingByTagName("h2");
            String pageName1 = driver.findElement(By.tagName("h2")).getText();
            System.out.println("Title of the page: " + pageName1);
            driver.navigate().refresh();
            waitingByTagName("h2");
            String pageName2 = driver.findElement(By.tagName("h2")).getText();
            if (pageName1.equals(pageName2)) {
                System.out.println("The page refresh is worked on this page");
            } else System.out.println("Not refreshed");

//           Return to the Dashboard page

            try {
                driver.findElement(By.id("header_logo")).click();
            } catch (Exception e) {
                driver.navigate().back();
                System.out.println("This page has differences from the others");
            }

            System.out.println();
            waitingByClassName("maintab");

//           Update of the WebElement list

            itemsMainMenu = driver.findElements(By.className("maintab"));
        }

    }
    public static void waitingByClassName(String className){
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public static void waitingByTagName(String tagName){
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.tagName(tagName)));
    }

    public static void waitingById(String idName){
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.id(idName)));
    }

}
